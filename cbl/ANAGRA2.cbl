      ******************************************************************
      * Licensed materials - Property of IBM                           *
      * 5724-T07 (c) Copyright IBM Corp. 2014                          *
      * All rights reserved                                            *
      * US Government users restricted rights  -  Use, duplication or  *
      * disclosure restricted by GSA ADP schedule contract with IBM    *
      * Corp.                                                          *
      *                                                                *
      * IBM Rational Developer for System z (RDz)                      *
      * IBM z/OS Automated Unit Testing Framework (zUnit)              *
      * Enterprise COBOL zUnit Test Case Sample AZUTC003               *
      * ANAGRA2.cbl                                                    *
      *                                                                *
      * This file contains a program ANAGRA2 that is part of the       *
      * Enterprise COBOL zUnit test case sample AZUTC003. ANAGRA2      *
      * tests if one word or phrase is an anagram of another, but it   *
      * has an intentional flaw where punctuation causes false         *
      * negatives.ANAGRA2 calls out a sub program SORTCHAR.            *
      *                                                                *
      * @since   9.1.0.0                                               *
      * @version 9.1.0.0                                               *
      ******************************************************************
       IDENTIFICATION DIVISION.
        PROGRAM-ID. 'ANAGRA2'.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WORD-BUF-1 PIC X(999).
       01 WORD-BUF-2 PIC X(999).
       01 CMPTMPA PIC S9(9) COMP-5.
       01 WS-SORT-PGM PIC X(8) VALUE 'SORTCHAR'.
       LINKAGE SECTION.
       COPY ANAGRAM.
       PROCEDURE DIVISION USING ANAGRMIO.
       MAINLINE SECTION.
           DISPLAY 'ANAGRAM STARTED...'
           PERFORM VALIDATE-INPUT
           PERFORM ANAGRAM-TEST
           DISPLAY 'ANAGRAM SUCCESSFUL'
           GOBACK
           .
       VALIDATE-INPUT.
           INITIALIZE STATUS-MSG-LEN STATUS-MSG
           IF FIRST-WORD-LEN = 0
             MOVE 1 TO CMPTMPA
             STRING 'ANAGRAM ERROR, FIRST-WORD-LEN IS ZERO.'
               DELIMITED BY SIZE INTO STATUS-MSG
               WITH POINTER CMPTMPA
             END-STRING
             COMPUTE STATUS-MSG-LEN = CMPTMPA - 1
             MOVE 16 TO RETURN-CODE
             GOBACK
           END-IF
           IF FIRST-WORD = SPACES
             MOVE 1 TO CMPTMPA
             STRING 'ANAGRAM ERROR, FIRST-WORD IS BLANK.'
               DELIMITED BY SIZE INTO STATUS-MSG
               WITH POINTER CMPTMPA
             END-STRING
             COMPUTE STATUS-MSG-LEN = CMPTMPA - 1
             MOVE 16 TO RETURN-CODE
             GOBACK
           END-IF
           IF SECOND-WORD-LEN = 0
             MOVE 1 TO CMPTMPA
             STRING 'ANAGRAM ERROR, SECOND-WORD-LEN IS ZERO.'
               DELIMITED BY SIZE INTO STATUS-MSG
               WITH POINTER CMPTMPA
             END-STRING
             COMPUTE STATUS-MSG-LEN = CMPTMPA - 1
             MOVE 16 TO RETURN-CODE
             GOBACK
           END-IF
           IF SECOND-WORD = SPACES
             MOVE 1 TO CMPTMPA
             STRING 'ANAGRAM ERROR, SECOND-WORD IS BLANK.'
               DELIMITED BY SIZE INTO STATUS-MSG
               WITH POINTER CMPTMPA
             END-STRING
             COMPUTE STATUS-MSG-LEN = CMPTMPA - 1
             MOVE 16 TO RETURN-CODE
             GOBACK
           END-IF
           .
       ANAGRAM-TEST.
           SET NOT-ANAGRAM TO TRUE
           INITIALIZE WORD-BUF-1
           MOVE FUNCTION UPPER-CASE(FIRST-WORD(1:FIRST-WORD-LEN))
             TO WORD-BUF-1(1:FIRST-WORD-LEN)
           DISPLAY
              'o FIRST-WORD:' FIRST-WORD(1:FIRST-WORD-LEN) '.'
           CALL WS-SORT-PGM USING FIRST-WORD-LEN WORD-BUF-1
           DISPLAY
              'o WORD-BUF-1:' WORD-BUF-1(1:FIRST-WORD-LEN) '.'
      *    INITIALIZE WORD-BUF-2
      *    MOVE FUNCTION UPPER-CASE(SECOND-WORD(1:SECOND-WORD-LEN))
      *       TO WORD-BUF-2(1:SECOND-WORD-LEN)
           MOVE FUNCTION UPPER-CASE(SECOND-WORD(1:SECOND-WORD-LEN))
              TO WORD-BUF-1(1:SECOND-WORD-LEN)
           DISPLAY
              'o SECOND-WORD:' SECOND-WORD(1:SECOND-WORD-LEN) '.'
           CALL WS-SORT-PGM USING SECOND-WORD-LEN WORD-BUF-1
           DISPLAY
             'o WORD-BUF-2:' WORD-BUF-2(1:SECOND-WORD-LEN) '.'
           COMPUTE CMPTMPA = FUNCTION MAX(FIRST-WORD-LEN,
                   SECOND-WORD-LEN)
           IF WORD-BUF-1(1:CMPTMPA) = WORD-BUF-2(1:CMPTMPA)
             SET IS-ANAGRAM TO TRUE
           END-IF
           DISPLAY 'o RESULT=' RESULT '.'
           .
       END PROGRAM 'ANAGRA2'.