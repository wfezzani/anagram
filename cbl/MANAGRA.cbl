      ******************************************************************
      * Licensed materials - Property of IBM                           *
      * 5724-T07 (c) Copyright IBM Corp. 2014                          *
      * All rights reserved                                            *
      * US Government users restricted rights  -  Use, duplication or  *
      * disclosure restricted by GSA ADP schedule contract with IBM    *
      * Corp.                                                          *
      *                                                                *
      ******************************************************************
       IDENTIFICATION DIVISION.
        PROGRAM-ID. 'MANAGRA'.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       COPY ANAGRAM.
       01 WS-ANAGRA-PGM PIC X(8) VALUE 'ANAGRA2'.
       PROCEDURE DIVISION.
       MAINLINE SECTION.
       MAINLINE-ENTRY.

           INITIALIZE ANAGRMIO
           PERFORM CANOE-OCEAN
           .
       MAINLINE-EXIT.
           GOBACK
           .

       CANOE-OCEAN SECTION.
       CANOE-OCEAN-ENTRY.

           MOVE 5 TO FIRST-WORD-LEN IN ANAGRMIO
           MOVE 'CANOES' TO FIRST-WORD IN ANAGRMIO
           MOVE 5 TO SECOND-WORD-LEN IN ANAGRMIO
           MOVE 'OCEAN' TO SECOND-WORD IN ANAGRMIO

           CALL WS-ANAGRA-PGM USING ANAGRMIO

           MOVE 10 TO FIRST-WORD-LEN IN ANAGRMIO
           MOVE 'DICTIONARY' TO FIRST-WORD IN ANAGRMIO
           MOVE 10 TO SECOND-WORD-LEN IN ANAGRMIO
           MOVE 'INDICATORY' TO SECOND-WORD IN ANAGRMIO

           CALL WS-ANAGRA-PGM USING ANAGRMIO
           .
       CANOE-OCEAN-EXIT.
           EXIT.

       END PROGRAM 'MANAGRA'.